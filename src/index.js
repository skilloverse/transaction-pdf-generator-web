import pdfMake from "pdfmake/build/pdfmake"
import pdfFonts from "pdfmake/build/vfs_fonts"
import { generateDocumentDef, TransactionPdfType } from 'transaction-pdf-generator-doc-def'
import equal from 'deep-equal'

pdfMake.vfs = pdfFonts.pdfMake.vfs

const qS = selector => document.querySelector(selector)
const qSA = selector => document.querySelectorAll(selector)

const throttleFactor = 2500

const getItemsDef = () => {
    const itemElms = qSA('.invoice-item')
    const items = []
    itemElms.forEach(elm => {
        const quantity = parseFloat(elm.querySelector('.item-quantity').value) || 0
        const description = elm.querySelector('.item-description').value
        const date = elm.querySelector('.item-date').value
        const unitPrice = parseFloat(elm.querySelector('.item-unit-price').value) || 0
        if (quantity || description || date || unitPrice) {
            items.push({
                quantity, description, date, unitPrice
            })
        }
    })
    return items
}

const getPaymentMethodsDef = () => {
    const paymentElms = qSA('.payment-method')
    const methods = []
    paymentElms.forEach(elm => {
        const name = elm.querySelector('.payment-method-name').value
        const details = elm.querySelector('.payment-method-details').value
        if (name || details) {
            methods.push({
                name, details
            })
        }
    })
    return methods
}

const getUpdatedDef = () => {
    const def = {}
    const logoPreview = qS('.logo-preview')
    if (logoPreview && logoPreview.complete && (logoPreview.naturalWidth !== 0)) {
        const canvas = document.createElement('canvas')
        const ctx = canvas.getContext('2d')
        canvas.width = logoPreview.width
        canvas.height = logoPreview.height
        ctx.drawImage(logoPreview, 0, 0)
        def.logo = {
            image: canvas.toDataURL(),
            fit: [parseInt(canvas.width / 2), parseInt(canvas.height / 2)],
        }
    }
    def.transactionPdfType = TransactionPdfType[qS('.document-type').value]
    def.companyDetails = qS('.company-details').value || undefined
    def.customerAddress = qS('.customer-address').value || undefined
    def.customerTaxId = qS('.customer-tax-id').value || undefined
    def.customerTaxIdType = qS('.customer-tax-id-type').value || undefined
    def.transactionId = qS('.transaction-id').value || undefined
    def.transactionDate = qS('.transaction-date').value || undefined
    def.paidDate = qS('.paid-date').value || undefined
    def.taxRate = parseFloat(qS('.tax-rate').value) || 0.0
    def.taxType = qS('.tax-type').value || undefined
    def.items = getItemsDef()
    def.currency = qS('.currency').value || undefined
    def.decimalPlaces = parseInt(qS('.decimal-places').value) || 2
    def.paymentMethods = getPaymentMethodsDef()
    def.supportText = qS('.support-text').value || undefined
    def.thankYouText = qS('.thank-you-text').value || undefined
    def.footerText = qS('.footer-text').value || undefined
    return def
}

let lastDef;
let lastUpdate;
let updateTimeoutId;
const throttledUpdatePdf = () => {
    if (!lastUpdate) {
        lastUpdate = Date.now()
        return updatePdf()
    }
    if (updateTimeoutId) {
        // will be updated by the timeout soon
        return
    }
    const timeSinceLast = Date.now() - lastUpdate
    if (timeSinceLast > throttleFactor) {
        lastUpdate = Date.now()
        return updatePdf()
    }
    else {
        updateTimeoutId = setTimeout(
            () => {
                updatePdf()
                updateTimeoutId = null
            },
            throttleFactor,
        )
    }
}

const updatePdf = () => {
    const def = getUpdatedDef()
    if (equal(def, lastDef)) {
        return
    }
    lastDef = def
    localStorage.setItem('doc-def', JSON.stringify(def))
    pdfMake.createPdf(generateDocumentDef(def)).getDataUrl(dataUrl => {
        const iframeContainer = document.querySelector('.output-iframe-container')
        iframeContainer.childNodes.forEach(node => {
            node.parentElement.removeChild(node)
        })
        const iframe = document.createElement('iframe')
        iframe.classList.add('output-iframe')
        iframe.src = dataUrl
        iframeContainer.appendChild(iframe)
    })
}

const controls = document.querySelectorAll('.input')
controls.forEach(control => {
    control.addEventListener('change', () => throttledUpdatePdf())
    control.addEventListener('keyup', () => throttledUpdatePdf())
})

const loadSavedData = () => {
    const data = localStorage.getItem('doc-def')
    if (!data) {
        addItem()
        addPaymentMethod()
        return
    }
    try {
        const def = JSON.parse(data)
        qS('.document-type').value = TransactionPdfType[def.transactionPdfType] || 'Invoice'
        qS('.company-details').value = def.companyDetails || ''
        qS('.customer-address').value = def.customerAddress || ''
        qS('.customer-tax-id').value = def.customerTaxId || ''
        qS('.customer-tax-id-type').value = def.customerTaxIdType || ''
        qS('.transaction-id').value = def.transactionId || ''
        qS('.transaction-date').value = def.transactionDate || ''
        qS('.paid-date').value = def.paidDate || ''
        qS('.tax-rate').value = def.taxRate || .0
        qS('.tax-type').value = def.taxType || ''
        if (!def.items || !def.items.length) {
            addItem()
        }
        else {
            def.items.forEach(item => addItem(item))
        }
        qS('.currency').value = def.currency || ''
        qS('.decimal-places').value = def.decimalPlaces || 2
        if (!def.paymentMethods || !def.paymentMethods.length) {
            addPaymentMethod()
        }
        else {
            def.paymentMethods.forEach(method => addPaymentMethod(method))
        }
        qS('.support-text').value = def.supportText || ''
        qS('.thank-you-text').value = def.thankYouText || ''
        qS('.footer-text').value = def.footerText || ''

    } catch (err) {
        addItem()
        addPaymentMethod()
        console.error(err)
    }

    qSA('.delete-invoice-item').forEach(item => {
        item.addEventListener('click', () => deleteItem(item))
    })
}

const deleteItem = item => {
    if (qSA('.invoice-item').length === 1) {
        item.querySelector('.item-quantity').value = 0
        item.querySelector('.item-description').value = ''
        item.querySelector('.item-date').value = ''
        item.querySelector('.item-unit-price').value = 0
    }
    else {
        item.parentElement.removeChild(item)
    }
}

const deletePaymentMethod = method => {
    if (qSA('.payment-method').length === 1) {
        method.querySelector('.payment-method-name').value = ''
        method.querySelector('.payment-method-details').value = ''
    }
    else {
        method.parentElement.removeChild(method)
    }
}

const addPaymentMethod = (methodDef = null) => {
    const name = document.createElement('input')
    name.setAttribute('type', 'text')
    name.setAttribute('size', 26)
    name.setAttribute('placeholder', 'Name, e.g. Bank Transfer')
    name.classList.add('payment-method-name')
    name.addEventListener('change', () => throttledUpdatePdf())
    name.addEventListener('keyup', () => throttledUpdatePdf())
    const details = document.createElement('textarea')
    details.setAttribute('rows', '4')
    details.setAttribute('cols', '30')
    details.setAttribute('placeholder', 'E.g. Send money to IBAN: DEXX...')
    details.classList.add('payment-method-details')
    details.addEventListener('change', () => throttledUpdatePdf())
    details.addEventListener('keyup', () => throttledUpdatePdf())
    if (methodDef) {
        name.value = methodDef.name
        details.value = methodDef.details
    }
    const deleteButton = document.createElement('button')
    deleteButton.textContent = 'Delete'
    const paymentMethod = document.createElement('div')
    paymentMethod.classList.add('payment-method')
    paymentMethod.appendChild(name)
    paymentMethod.appendChild(document.createElement('br'))
    paymentMethod.appendChild(details)
    paymentMethod.appendChild(document.createElement('br'))
    paymentMethod.appendChild(deleteButton)
    deleteButton.addEventListener('click', () => deletePaymentMethod(paymentMethod))
    qS('.payment-methods').appendChild(paymentMethod)
}

const addItem = (itemDef = null) => {
    const quantity = document.createElement('input')
    quantity.setAttribute('type', 'number')
    quantity.setAttribute('placeholder', 'Quantity')
    quantity.classList.add('item-quantity')
    quantity.addEventListener('change', () => throttledUpdatePdf())
    quantity.addEventListener('keyup', () => throttledUpdatePdf())
    const description = document.createElement('input')
    description.setAttribute('type', 'text')
    description.setAttribute('size', 25)
    description.setAttribute('placeholder', 'Description')
    description.classList.add('item-description')
    description.addEventListener('change', () => throttledUpdatePdf())
    description.addEventListener('keyup', () => throttledUpdatePdf())
    const date = document.createElement('input')
    date.setAttribute('type', 'text')
    date.setAttribute('size', 10)
    date.setAttribute('placeholder', 'Date')
    date.classList.add('item-date')
    date.addEventListener('change', () => throttledUpdatePdf())
    date.addEventListener('keyup', () => throttledUpdatePdf())
    const unitPrice = document.createElement('input')
    unitPrice.setAttribute('type', 'number')
    unitPrice.setAttribute('step', '0.01')
    unitPrice.setAttribute('placeholder', 'Unit Price')
    unitPrice.classList.add('item-unit-price')
    unitPrice.addEventListener('change', () => throttledUpdatePdf())
    unitPrice.addEventListener('keyup', () => throttledUpdatePdf())
    if (itemDef) {
        quantity.value = itemDef.quantity
        description.value = itemDef.description
        date.value = itemDef.date
        unitPrice.value = itemDef.unitPrice
    }
    const deleteButton = document.createElement('button')
    deleteButton.textContent = 'Delete'
    const invoiceItem = document.createElement('div')
    invoiceItem.classList.add('invoice-item')
    invoiceItem.appendChild(quantity)
    invoiceItem.appendChild(document.createTextNode(' '))
    invoiceItem.appendChild(description)
    invoiceItem.appendChild(document.createTextNode(' '))
    invoiceItem.appendChild(date)
    invoiceItem.appendChild(document.createTextNode(' '))
    invoiceItem.appendChild(unitPrice)
    invoiceItem.appendChild(document.createTextNode(' '))
    invoiceItem.appendChild(deleteButton)
    deleteButton.addEventListener('click', () => deleteItem(invoiceItem))
    qS('.invoice-items').appendChild(invoiceItem)
}

qS('.add-item-button').addEventListener('click', () => {
    addItem()
})

qS('.add-payment-method-button').addEventListener('click', () => {
    addPaymentMethod()
})

const loadLogoPreview = () => {
    const logoInput = qS('.company-logo')
    const preview = qS('.logo-preview-container')
    preview.childNodes.forEach(node => node.parentElement.removeChild(node))
    const image = document.createElement('img')
    image.classList.add('logo-preview')
    image.addEventListener('load', () => throttledUpdatePdf())
    image.src = URL.createObjectURL(logoInput.files[0])
    preview.appendChild(image)
}

qS('.company-logo').addEventListener('change', () => {
    loadLogoPreview()
})

if (qS('.company-logo').files && qS('.company-logo').files.length) {
    loadLogoPreview()
}

loadSavedData()
throttledUpdatePdf()
