# transaction-pdf-generator-web

Generates invoice and receipt PDFs for transaction records in a browser.

## Setup

### Prerequisites
- Yarn and its dependencies are installed

### Setup steps

1. Clone/download this project and enter its directory.
1. Run `yarn install`
1. Run `yarn build`
1. Run `yarn start`. The URL to where the project runs will be shown in the
browser. Open it to create invoices on your computer.
1. Enter invoice details into the webpage form on the left and the invoice will
be generated on the right.

## Demo

You can a demo of this project running here:
[skilloverse.com/invoice-receipt-pdf-generator](https://www.skilloverse.com/invoice-receipt-pdf-generator/index.html)
